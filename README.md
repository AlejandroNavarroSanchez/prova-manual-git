# Barcelona

---

Text amb informació de Barcelona, bonica ciutat del Mediterrani

## Llista ordenada de barris
1. Noubarris
2. Guinardo
3. Trinitat

## Llista desordenada de barris
- Trinitat Nova
- Guineuta
- Ciutat Vella

## Enllaç a una web [enllaç a barcelona](https://www.barcelona.com/)

## BARCELONA

---

![Silueta barcelona](https://img.freepik.com/free-vector/barcelona-city-colored-gradient-line_310772-200.jpg?size=626&ext=jpg "Barcelona")
